import { _decorator, Component, Node } from 'cc';
import { GameUtils } from './utils/GameUtils';
import { Scene } from 'cc';
import { Prefab } from 'cc';
import { instantiate } from 'cc';
import { ListGameItem } from './components/ListGameItem';
import { screen } from 'cc';
import { Canvas } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Game')
export class Game extends Component {

    @property(Node)
    listGameContent: Node;
    @property(Prefab)
    listGameItemPrefab: Prefab;

    protected onLoad(): void {
        Canvas
        screen.fullScreen();
        console.log()
        GameUtils.loadGameConfig((data: { name: string, scene: string }[]) => {
            data.forEach((gameData) => {
                const listGameItemNode = instantiate(this.listGameItemPrefab);
                listGameItemNode.parent = this.listGameContent;
                listGameItemNode.getComponent(ListGameItem).init(gameData);
            })
        })
    }

    start() {

    }

    update(deltaTime: number) {

    }
}



import { _decorator, Component, Node } from 'cc';
import ViewUtils from '../utils/ViewUtils';
import { Widget } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('ResizeComp')
export class ResizeComp extends Component {

    protected onLoad(): void {
        this.node.getComponent(Widget) && (this.node.getComponent(Widget).enabled = false);
        ViewUtils.scaleNodeSizeToScreenSize(this.node)
    }
    start() {

    }

    update(deltaTime: number) {
        
    }
}



import { Button } from 'cc';
import { tween } from 'cc';
import { CCBoolean } from 'cc';
import { TweenSystem } from 'cc';
import { Vec3 } from 'cc';
import { Sprite } from 'cc';
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('SwitchComponent')
export class SwitchComponent extends Button {

    @property(Sprite)
    offSprite: Sprite;
    @property(Sprite)
    onSprite: Sprite;
    private _isOn: boolean = false;
    public get isOn(): boolean {
        return this._isOn;
    }
    @property({type: CCBoolean})
    public set isOn(value: boolean) {
        this._isOn = value;
        this.updateUI();
    }

    protected readonly offPosition: Vec3 = new Vec3();
    protected readonly onPosition: Vec3 = new Vec3();

    private _onChangeListener: (value: boolean) => void;
    public set onChangeListener(value: (value: boolean) => void) {
        this._onChangeListener = value;
    }

    protected onLoad(): void {
        this.offPosition.set(this.offSprite.node.position);
        this.onPosition.set(this.onSprite.node.position);
        this.updateUI();
    }

    onEnable(): void {
        super.onEnable();
        this.node.on('click', this.onClick, this);
    }
    onDisable(): void {
        super.onDisable();
        this.node.off('click', this.onClick, this);
    }

    onClick() {
        this.isOn = !this._isOn;
    }
    updateUI() {
        // this.offSprite.node.active = !this._isOn;
        // this.onSprite.node.active = this._isOn;
        TweenSystem.instance.ActionManager.removeAllActionsFromTarget(this.offSprite.node);
        TweenSystem.instance.ActionManager.removeAllActionsFromTarget(this.onSprite.node);
        let startNode = this._isOn ? this.offSprite.node : this.onSprite.node;
        let endNode = this._isOn ? this.onSprite.node : this.offSprite.node;
        
        let startPosition = this._isOn ? this.offPosition.clone() : this.onPosition.clone();
        let endPosition = this._isOn ? this.onPosition.clone() : this.offPosition.clone();

        startNode.active = true;
        endNode.active = false;

        tween(startNode).to(0.2, { position: endPosition }, { easing: 'backInOut' })
        .call(() => {
            startNode.active = false;
            endNode.active = true;
            startNode.setPosition(startPosition.clone());
            this._onChangeListener &&  this._onChangeListener(this._isOn);
        }).start();
    }

}



import { director } from 'cc';
import { Scene } from 'cc';
import { Label } from 'cc';
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('ListGameItem')
export class ListGameItem extends Component {

    @property(Label)
    gameNameLabel: Label;

    private gameScene: string;
    init(data: {name: string, scene: string}) {
        this.gameNameLabel.string = data.name;
        this.gameScene = data.scene;
    }
    start() {

    }

    update(deltaTime: number) {
        
    }

    onClick() {
        if (!this.gameScene) return;
        director.preloadScene(this.gameScene, (err) => {
            if (err) {
                console.error(`Failed to preload scene: ${this.gameScene}, ${err}`);
                return;
            }
            director.loadScene(this.gameScene);
        });
    }
}



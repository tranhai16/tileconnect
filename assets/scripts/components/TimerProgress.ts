// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, ProgressBar } from "cc";
import GameManager from "../GameManager";
import TimerManager from "../utils/TimerManager";

const {ccclass, property} = _decorator;

@ccclass
export default class TimerProgress extends Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    update (dt: number) {
        this.node.getComponent(ProgressBar).progress = TimerManager.instance.progress;
    }
}

// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:

import { _decorator, Sprite, Component, SpriteFrame, Node, CCFloat, UITransform } from "cc";

//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
const {ccclass, property, requireComponent} = _decorator;
@ccclass
@requireComponent(Sprite)
export default class ResizeSprite extends Component {


    @property(Node)
    parentNode: Node;

    private _scaleFactor: number = 0.75;
    public get scaleFactor(): number {
        return this._scaleFactor;
    }
    @property({type: CCFloat})
    public set scaleFactor(value: number) {
        this._scaleFactor = value;
        this._resize();
    }

    private _currentSF : SpriteFrame;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    private _resize() {
        if (!this.node.getComponent(Sprite)) return;
        if (!this.node.getComponent(Sprite).spriteFrame) return;
        this._currentSF = this.node.getComponent(Sprite).spriteFrame;
        let size = this._currentSF.rect.clone().size;
        let bound = this.parentNode.getComponent(UITransform).contentSize.clone();
        let ratio = size.width / size.height;
        if (ratio >= 1) {
            size.width = bound.width * this.scaleFactor;
            size.height = size.width / ratio;
        }
        else {
            size.height = bound.height * this.scaleFactor;
            size.width = size.height * ratio;
        }
        this.node.getComponent(UITransform).setContentSize(size);
    }

    update (dt : number) {
        if (this._currentSF !== this.node.getComponent(Sprite)!.spriteFrame) {
            this._resize();
        }
    }
}

// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, SpriteFrame, Node, Prefab } from "cc";
import GameManager, {  } from "./GameManager";
import AudioManager from "./utils/AudioManager";
import { AuidoKey } from "./utils/Const";
import { GameUtils, ShowEffect } from "./utils/GameUtils";
import LevelConfig from "./utils/LevelConfig";
import MainView from "./views/MainView";
import ViewUtils from "./utils/ViewUtils";

const {ccclass, property} = _decorator;

@ccclass
export default class Main extends Component {
    // @property(AudioSource)
    // exellentSource: AudioSource;
    // @property(AudioSource)
    // wrongSource: AudioSource;

    mainView: MainView;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        ViewUtils.gameRoot = this.node;
        AudioManager.instance.init(this.node);
        AudioManager.instance.addAudio(AuidoKey.MUSIC, 'raws/music', true, true);
        AudioManager.instance.addAudio(AuidoKey.CLICK, 'raws/touch');
        AudioManager.instance.addAudio(AuidoKey.EXELLENT, 'raws/exellent');
        AudioManager.instance.addAudio(AuidoKey.WRONG, 'raws/wrong');
        AudioManager.instance.muteAll = false;
        // AudioManager.instance.play(AuidoKey.MUSIC, true);
        Promise.all([
            GameUtils.loadLevelConfig(),
            GameUtils.loadAnimalSprites(),
            GameUtils.loadPrefab('prefabs/animalTile'),
            GameUtils.loadPrefabToNode('prefabs/main', this.node, ShowEffect.FROM_BOTTOM),
        ]).then((value : [any, SpriteFrame[], Prefab, Node]) => {
            GameManager.instance.levelConfigs = [];
            (value[0] as any).forEach((v : any) => {
                GameManager.instance.levelConfigs.push(new LevelConfig().parseData(v));
            })
            GameManager.instance.spriteFrameData = value[1] as SpriteFrame[];
            this.mainView = value[3].getComponent(MainView);
            GameManager.instance.initPrefab(value[2], value[3].getChildByName('drawNode'));
            this.mainView.reloadUI();
            // this.node.addChild(value[2]);
        }).catch((error : Error) => { console.log(error); });
    }

    protected onEnable(): void {
        GameManager.instance.eventTarget.on('new-game', this.addNewGame, this);
    }
    protected onDisable(): void {
        GameManager.instance.eventTarget.off('new-game', this.addNewGame, this);
    }
    addNewGame(): void {
        this.mainView && this.mainView.reloadUI();
    }
    start () {
    }

    // update (dt) {}
}

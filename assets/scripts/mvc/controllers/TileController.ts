import { Size } from "cc";
import Tile from "../models/Tile";
import TileView from "../views/TileView";
import AudioManager from "../../utils/AudioManager";
import { AuidoKey } from "../../utils/Const";
import GameManager from "../../GameManager";
import Algorithm from "../../utils/Algorithm";
import { Prefab } from "cc";
import { v2 } from "cc";
import { Vec2 } from "cc";
import AdmobManager from "../../managers/AdmobManager";
import { log } from "cc";
import AdConst from "../../const/AdConst";
import { sys } from "cc";

export default class TileController {

    private models: Tile[] = [];
    private _views: TileView[] = [];
    public get views(): TileView[] {
        return this._views;
    }
    private _algorithm: Algorithm;
    private _connectData: number[] = [];
    private _linePoints: Vec2[] = [];

    constructor(algorithm: Algorithm, prefab: Prefab, tileSize: Size) {
        this._algorithm = algorithm;
        this.models = [];
        this._views = [];
        for (let i = 0; i < this._algorithm.matrix.length; i++) {
            for (let j = 0; j < this._algorithm.matrix[i].length; j++) {
                if (this._algorithm.getData(i, j) !== -1) {
                    let model = new Tile(this._algorithm.getId(i, j));
                    model.x = i;
                    model.y = j;
                    model.tileSize = tileSize.clone();
                    let view = new TileView(prefab);
                    view.idx = model.id;
                    view.frameData = this._algorithm.getData(i, j);
                    view.setOnClickListener((view: TileView) => {
                        this.onClick(view);
                    })
                    this.models.push(model);
                    this.views.push(view);
                }
            }
        }
        this._views.forEach((view: TileView) => {
            view.update(this.models.find(model => model.id === view.idx));
        })
    }
    onClick(view: TileView) {
        AudioManager.instance.play(AuidoKey.CLICK);
        view.tileComponent.hideHint();
        if (view.isSelected) {
            this._connectData.push(view.idx);
            if (this._connectData.length === 2) {
                this._linePoints = [];
                let view1 = this.views.find(view => view.idx === this._connectData[0]);
                let view2 = this.views.find(view => view.idx === this._connectData[1]);
                let model1 = this.models.find(model => model.id === this._connectData[0]);
                let model2 = this.models.find(model => model.id === this._connectData[1]);
                let matching = this.checkConnect(model1, model2);
                if (!matching) {
                    view1.unselect();
                    view2.unselect();
                    GameManager.instance.eventTarget.emit('play-wrong');
                }
                else {
                    let startPosition = view1.node.getPosition();
                    matching.forEach((point) => {
                        let normalized = point.clone().subtract2f(model1.x, model1.y);
                        let linePoint = new Vec2(startPosition.x + normalized.x * model1.tileSize.width, startPosition.y + normalized.y * model1.tileSize.height);
                        this._linePoints.push(linePoint);
                    });
                    GameManager.instance.eventTarget.emit('draw-line', this._linePoints);
                    this._algorithm.removes([v2(model1.x, model1.y), v2(model2.x, model2.y)]);
                    this.updateModels();
                    Promise.all([this.remove(view1), this.remove(view2)]).then(([v1, v2]) => {
                        let prms: Promise<void>[] = [];
                        this._views.forEach(v => {
                            let md = this.models.find(m => m.id === v.idx);
                            if (md && this._algorithm.getData(md.x, md.y) !== -1) {
                                prms.push(v.update(md));
                            }
                        })
                        Promise.all(prms).then(() => {
                            GameManager.instance.updateUI();
                            GameManager.instance.eventTarget.emit('play-exellent');
                            GameManager.instance.eventTarget.emit('clear-line');
                            this.addScore();
                        })

                    });
                }
                this._connectData = [];
            }
        }
        else {
            this._connectData.pop();
        }
        view.updateSelected(view.isSelected);
    }
    addScore() {
        GameManager.instance.userData.current_score += 1;
        sys.localStorage.setItem('user-data', JSON.stringify(GameManager.instance.userData));
        GameManager.instance.eventTarget.emit("update-score");
    }
    updateModels() {
        for (let i = 0; i < this._algorithm.matrix.length; i++) {
            for (let j = 0; j < this._algorithm.matrix[i].length; j++) {
                this.models.find(model => model.id === this._algorithm.matrix[i][j]).set(i, j);
                // if (this._algorithm.getData(i, j) === -1) {
                //     let view_delete = this.views.find(view => view.idx === this._algorithm.getId(i, j));
                //     if (view_delete) {
                //         this.views.splice(this.views.indexOf(view_delete), 1);
                //     }
                // }
            }
        }
    }
    remove(view: TileView): Promise<void> {
        return view.remove();
    }
    checkConnect(model1: Tile, model2: Tile): Vec2[] | null {
        if (this._algorithm.getData(model1.x, model1.y) !== this._algorithm.getData(model2.x, model2.y)) {
            console.log("not matching");
            return null;
        }
        let path = this._algorithm.canConnect(v2(model1.x, model1.y), v2(model2.x, model2.y));
        if (!path) return null;
        console.log("match");

        return path;
    }

    showHint(user_hint_number: number, callback: (rewarded: boolean) => void): void {
        if (user_hint_number > 0) {
            let hint = this._algorithm.suggest[0];
            if (!hint) {
                console.log('No more hints available');
                return;
            }
            let startData = hint.start;
            let endData = hint.end;
    
            let startModel = this.models.find(model => model.equals(startData));
            let endModel = this.models.find(model => model.equals(endData));
            if (!startModel || !endModel) return;
            let startView = this.views.find(view => view.idx === startModel.id);
            let endView = this.views.find(view => view.idx === endModel.id);
            if (!startView || !endView) return;
            startView.tileComponent.showHint();
            endView.tileComponent.showHint();
            callback(false);
            return;
        }
        AdmobManager.instance.showRewardedAds(AdConst.AdsConfigs.REWARD_USE_HINT_TILE_CONNECT, (data: {type: string, amount: number}) => {
            if (AdConst.TEST) {
                callback(true);
                return;
            }
            if (!data) {
                callback(false);
                return;
            }
            if (data.type === 'hint' && data.amount > 0) {
                callback(true);
                return;
            }
            callback(false)

        })
        
    }

}
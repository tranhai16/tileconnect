import { v3 } from "cc";
import { TileComponent } from "../../views/TileComponent";
import Tile from "../models/Tile";
import { tween } from "cc";
import { TweenSystem } from "cc";
import { Node } from "cc";
import { Prefab } from "cc";
import { instantiate } from "cc";
import { SpriteFrame } from "cc";

export default class TileView {
    

    private _isSelected: boolean = false;
    public get isSelected(): boolean {
        return this._isSelected;
    }
    public set isSelected(value: boolean) {
        this._isSelected = value;
    }

    private _tileComponent: TileComponent;
    public get tileComponent(): TileComponent {
        return this._tileComponent;
    }

    private _idx: number;
    public get idx(): number {
        return this._idx;
    }
    public set idx(value: number) {
        this._idx = value;
    }

    private _frameData: number;
    public get frameData(): number {
        return this._frameData;
    }
    public set frameData(value: number) {
        this._frameData = value;

    }
    private _node: Node;
    public get node(): Node {
        return this._node;
    }

    setOnClickListener(onClick: (view: TileView) => void): void {
        this._tileComponent.onClickListener = () => {
            this._isSelected = !this._isSelected;
            onClick && onClick(this);
        }
    }

    constructor(prefab: Prefab) {
        this._node = instantiate(prefab);
        this._tileComponent = this.node.getComponent(TileComponent);
    }

    setSpriteFrame(frame: SpriteFrame) {
        this.tileComponent.tileSprite.spriteFrame = frame;
    }

    update(model: Tile) : Promise<void> {
        if (!this._node || !this._tileComponent) return Promise.resolve();
        return this.updatePosition(model);

    }
    updateSelected(isSelected) {
        if (!this._node || !this._tileComponent) return;
        if (isSelected) {
            this._tileComponent.normal();
        } else {
            this._tileComponent.gray();
        }
    }

    unselect() {
        this._isSelected = false;
        this.updateSelected(false);
    }

    private updatePosition(model: Tile) : Promise<void> {
        if (!this._node) return Promise.resolve();
        let pos = v3((model.x + 0.5 - 8) * model.tileSize.width, (model.y + 0.5 - 4) * model.tileSize.height);
        if (pos.equals(this._node.position)) {
            return Promise.resolve();
        }
        TweenSystem.instance.ActionManager.removeAllActionsFromTarget(this._node);
        return new Promise<void>((resolve, reject) => {
            tween(this._node).to(0.2, { position: pos }, { easing: 'backInOut' })
            .call(() => {resolve()}).start();
        })
        // tween(this._node).to(0.2, { position: pos }, { easing: 'backInOut' }).start();
    }

    remove() : Promise<void> {
        return new Promise((rs, rj) => {
            tween(this._node).to(0.1, { scale: v3(0, 0, 0) }, { easing: 'bounceInOut' }).call(() => {
                this._tileComponent.remove();
                rs(null);
            }).start();
        })
        
    }
}
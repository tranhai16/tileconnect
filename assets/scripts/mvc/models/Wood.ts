import { math } from "cc";
import { Vec3 } from "cc";
import { v3 } from "cc";
import ArrangeWoodManager from "../../managers/ArrangeWoodManager";

export default class Wood {
    allPosition(): Vec3[] {
        let result: Vec3[] = [];
        this._mapPos.forEach(p => result.push(p.clone()));
        return result;
    }
    private _mapPos: Map<number, Vec3> = new Map<number, Vec3>();
    config: {depths: number, rows: number, cols: number} = {
        depths: 3,
        rows: 3,
        cols: 3,
    }
    matrix: number[][][] = [
        [
            [0, 0, 0],
            [-1, 0, 0],
            [0, 0, 0],
        ],
        [
            [0, 0, 0],
            [-1, -1, 0],
            [0, 0, 0],
        ],
        [
            [0, 0, 0],
            [-1, 0, 0],
            [0, 0, 0],
        ],
    ]

    constructor() {
        for (let z = 0; z < this.matrix.length; z++) {
            for (let x = 0; x < this.matrix[z].length; x++) {
                for (let y = 0; y < this.matrix[z][x].length; y++) {
                    if (this.matrix[z][x][y] === -1) {
                        this.matrix[z][x][y] = ArrangeWoodManager.instance.generateId();
                        let position = new Vec3(x, y, z);
                        this._mapPos.set(this.matrix[z][x][y], position);
                    }
                }
            }
        }
        this.allPosition().forEach((position) => {
            console.log(position.clone().subtract3f(1, 1, 1));
        })
        // console.log();
    }

    

    get(x: number, y: number, z: number) {
        return this.matrix[z][x][y];
    }

    position(id: number) : Vec3 {
        if (id === 0) return null;
        return this._mapPos.get(id);
    }
    rotateX(deg: number) {
        if (deg % 90 !== 0) {
            console.error("angle must divide 90 degrees");
        }
        let newMatrix = Array.from({ length: this.config.depths }, () =>
            Array.from({ length: this.config.rows }, () =>
                Array.from({ length: this.config.cols }, () => 0)
            )
        );
        for (let z = 0; z < newMatrix.length; z++) {
            for (let x = 0; x < newMatrix[z].length; x++) {
                for (let y = 0; y < newMatrix[z][x].length; y++) {
                    let position = v3(x, y, z);
                    let value = this.matrix[z][x][y];
                    let newPosition = new Vec3();
                    Vec3.rotateX(newPosition, position, v3(1, 1, 1), math.toRadian(deg));
                    newPosition.x = Math.round(newPosition.x);
                    newPosition.y = Math.round(newPosition.y);
                    newPosition.z = Math.round(newPosition.z);
                    newMatrix[newPosition.z][newPosition.x][newPosition.y] = value;
                    if (this._mapPos.has(value)) {
                        this._mapPos.set(value, newPosition.clone());
                    }
                }
            }
        }
        this.matrix = newMatrix;
        console.log(this.allPosition());
        return newMatrix;
    }
    rotateY(deg: number) {
        if (deg % 90 !== 0) {
            console.error("angle must divide 90 degrees");
        }
        let newMatrix = Array.from({ length: this.config.depths }, () =>
            Array.from({ length: this.config.rows }, () =>
                Array.from({ length: this.config.cols }, () => 0)
            )
        );
        for (let z = 0; z < newMatrix.length; z++) {
            for (let x = 0; x < newMatrix[z].length; x++) {
                for (let y = 0; y < newMatrix[z][x].length; y++) {
                    let position = v3(x, y, z);
                    let value = this.matrix[z][x][y];
                    let newPosition = new Vec3();
                    Vec3.rotateY(newPosition, position, v3(1, 1, 1), math.toRadian(deg));
                    newPosition.x = Math.round(newPosition.x);
                    newPosition.y = Math.round(newPosition.y);
                    newPosition.z = Math.round(newPosition.z);
                    newMatrix[newPosition.z][newPosition.x][newPosition.y] = value;
                    if (this._mapPos.has(value)) {
                        this._mapPos.set(value, newPosition.clone());
                    }
                }
            }
        }
        this.matrix = newMatrix;
        console.log(this.allPosition());
        return newMatrix;
    }
    rotateZ(deg: number) {
        if (deg % 90 !== 0) {
            console.error("angle must divide 90 degrees");
        }
        let newMatrix = Array.from({ length: this.config.depths }, () =>
            Array.from({ length: this.config.rows }, () =>
                Array.from({ length: this.config.cols }, () => 0)
            )
        );
        for (let z = 0; z < newMatrix.length; z++) {
            for (let x = 0; x < newMatrix[z].length; x++) {
                for (let y = 0; y < newMatrix[z][x].length; y++) {
                    let position = v3(x, y, z);
                    let value = this.matrix[z][x][y];
                    let newPosition = new Vec3();
                    Vec3.rotateZ(newPosition, position, v3(1, 1, 1), math.toRadian(deg));
                    newPosition.x = Math.round(newPosition.x);
                    newPosition.y = Math.round(newPosition.y);
                    newPosition.z = Math.round(newPosition.z);
                    newMatrix[newPosition.z][newPosition.x][newPosition.y] = value;
                    if (this._mapPos.has(value)) {
                        this._mapPos.set(value, newPosition.clone());
                    }
                }
            }
        }
        this.matrix = newMatrix;
        console.log(this.allPosition());
        return newMatrix;
    }
}

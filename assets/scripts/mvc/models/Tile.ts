import { Size } from "cc";
import Algorithm from "../../utils/Algorithm";

export default class Tile {
    set(x: number, y: number) {
        this._x = x;
        this._y = y;
    }
    private readonly _id: number;
    public get id(): number {
        return this._id;
    }
    private _x: number;
    public get x(): number {
        return this._x;
    }
    public set x(value: number) {
        this._x = value;
    }
    private _y: number;
    public get y(): number {
        return this._y;
    }
    public set y(value: number) {
        this._y = value;
    }

    private _tileSize: Size;
    public get tileSize(): Size {
        return this._tileSize;
    }
    public set tileSize(value: Size) {
        this._tileSize = value;
    }
    

    constructor(id: number) {
        this._id = id;
    }

    equals(other: Tile | number, y?: number): boolean {
        if (other instanceof Tile) {
            return this._id === other._id;
        }
        if (!y) return this._id === other;
        return this.x === other && this.y === other;
    }
}
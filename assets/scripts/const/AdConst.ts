
export type AdsConfig = {
    readonly REWARD_USE_HINT_TILE_CONNECT: string,
    readonly INTER_GO_TILE: string,
    readonly INTER_GO_ARRANGE_WOOD: string,
}

export default class AdConst {

    static get TEST(): boolean {
        //@ts-ignore
        return CC_DEBUG;
    };

    private static readonly testAds : AdsConfig = {
        REWARD_USE_HINT_TILE_CONNECT: 'ca-app-pub-3940256099942544/5354046379',
        INTER_GO_TILE: 'ca-app-pub-3940256099942544/1033173712',
        INTER_GO_ARRANGE_WOOD: 'ca-app-pub-3940256099942544/1033173712',
    }
    private static readonly realAds : AdsConfig = {
        REWARD_USE_HINT_TILE_CONNECT: '',
        INTER_GO_TILE: '',
        INTER_GO_ARRANGE_WOOD: '',
    }

    static get AdsConfigs(): AdsConfig {
        return AdConst.TEST? AdConst.testAds : AdConst.realAds;
    }

}

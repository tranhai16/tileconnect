import { Vec2, v2, Node } from "cc";

export default class Algorithm {
    remove(point: Vec2) {
        this._mapFrame.set(this._matrix[point.x][point.y], -1);
    }
    removes(point: Vec2[]) {
        point.forEach((p) => {
            this.remove(p)
        });
        point.forEach((p) => {
            if (this._goCenterHorizontal) {
                this.updateMoveToCenterHorizontally(p.x);
            }
            if (this._goCenterVertical) {
                this.updateMoveToCenterVertically(p.y);
            }
        })
        this.findSuggest();
        
    }

    private findSuggest() {
        let data: Vec2[] = [];
        for (let i = 0; i < this._rows; i++) {
            for (let j = 0; j < this._cols; j++) {
                if (this._mapFrame.get(this._matrix[i][j]) !== -1) {
                    data.push(v2(i, j));
                }
            }
        }
        this._suggest = [];
        if (data.length === 0) {
            this._isEmpty = true;
            return;
        }
        for (let i = 0; i < data.length - 1; i++) {
            for (let j = i + 1; j < data.length; j++) {
                if (this.getData(data[i].x, data[i].y) === this.getData(data[j].x, data[j].y)
                    && this.canConnect(data[i], data[j])) {
                    this._suggest.push({ start: this.getId(data[i].x, data[i].y), end: this.getId(data[j].x, data[j].y) });
                    return;
                }
            }
        }
        if (!this._isEmpty && this._suggest.length === 0) {
            console.log('Not found a suggestion');
            this.shuffleMatrix(this._matrix);
        }
    }
    shuffleMatrix(_matrix: number[][]) {
        for (let i = 0; i < _matrix.length; i++) {
            for (let j = 0; j < _matrix[i].length; j++) {
                const temp = _matrix[i][j];
                if (this._mapFrame.get(_matrix[i][j]) === -1) continue;
                const randomIndex = Math.floor(Math.random() * _matrix[i].length);
                if (this._mapFrame.get(_matrix[i][randomIndex]) === -1) continue;
                _matrix[i][j] = _matrix[i][randomIndex];
                _matrix[i][randomIndex] = temp;
            }
        }
        this.findSuggest();
    }
    private _matrix: number[][] = [];
    public get matrix(): number[][] {
        return this._matrix;
    }
    private _mapFrame: Map<number, number>;
    public get mapFrame(): Map<number, number> {
        return this._mapFrame;
    }
    private _cols: number;
    public get cols(): number {
        return this._cols;
    }
    private _rows: number;
    public get rows(): number {
        return this._rows;
    }

    private _genId: number[] = [];
    private _suggest: { start: number; end: number; }[] = [];
    public get suggest(): { start: number; end: number; }[] {
        return this._suggest;
    }
    private _isEmpty: boolean = false;
    public get isEmpty(): boolean {
        return this._isEmpty;
    }
    private _lastTileRemoveds: number[] = [];
    public get lastTileRemoveds(): number[] {
        return this._lastTileRemoveds;
    }


    private static _instance: Algorithm;
    public static get instance(): Algorithm {
        if (!this._instance) {
            this._instance = new Algorithm();
        }
        return Algorithm._instance;
    }


    private constructor() {

    }

    private _goCenterHorizontal: boolean = false;
    private _goCenterVertical: boolean = false;

    generateId(): number {
        if (this._genId.length === 0) {
            this._genId.push(1);
            return 1;
        }
        let id = this._genId[this._genId.length - 1] + 1;
        this._genId.push(id);
        return id;
    }

    setGoCenter(go_center_horizontal: boolean, go_center_vertical: boolean) {
        this._goCenterHorizontal = go_center_horizontal;
        this._goCenterVertical = go_center_vertical;
    }

    generate(n_rows: number, n_columns: number, n_types: number, count: number[]) {
        this._isEmpty = false;
        this._rows = n_rows;
        this._cols = n_columns;
        this._genId = [];
        this._matrix = [];
        this._mapFrame = new Map();
        const countType: Map<number, number> = new Map();
        for (let type = 0; type < n_types; ++type) {
            countType.set(type, 0);
        }

        for (let i = 0; i < this._rows; ++i) {
            let col: number[] = [];
            for (let j = 0; j < this._cols; ++j) {
                let type: number;
                do {
                    type = Math.floor(Math.random() * n_types);
                } while (countType.get(type)! >= count[type]);

                countType.set(type, countType.get(type)! + 1);
                let id = this.generateId();
                col.push(id);
                this._mapFrame.set(id, type);
                // this._matrixTiles[i][j] = new Tile()
            }
            this._matrix.push(col);

        }
    }

    getId(row: number, col: number) {
        return this._matrix[row][col];
    }
    getData(row: number, col: number) {
        return this._mapFrame.get(this.getId(row, col));
    }
    private printArr() {
        let str = '';
        for (let i = 0; i < this._matrix.length; i++) {
            for (let j = 0; j < this._matrix[i].length; j++) {
                str += (this._mapFrame.get(this._matrix[i][j]) < 10 ? ('0' + this._mapFrame.get(this._matrix[i][j])) : this._mapFrame.get(this._matrix[i][j])) + '\t';
            }
            str += '\n';
        }
        console.log(str);
    }

    private findPath(_x: number, _y: number, x: number, y: number): Pair[] {
        let matrix :number[][] = [];
        for (let i = 0; i < this._matrix.length; i++) {
            let col : number[] = [];
            for (let j = 0; j < this._matrix[i].length; ++j) {
                col.push(this._mapFrame.get(this._matrix[i][j]));
            }
            matrix.push(col);
        }
        // INIT Graph
        const e: number[][] = Array.from({ length: this._rows + 2 }, () => Array(this._cols + 2).fill(0));
        for (let i = 0; i < this._rows; ++i) {
            for (let j = 0; j < this._cols; ++j) {
                e[i + 1][j + 1] = matrix[i][j] !== -1 ? 1 : 0;
            }
        }
        const s: Pair = [_x + 1, _y + 1];
        const t: Pair = [x + 1, y + 1];

        // BFS
        const dx: number[] = [-1, 0, 1, 0];
        const dy: number[] = [0, 1, 0, -1];
        const q: Pair[] = [];
        const trace: Pair[][] = Array.from({ length: e.length }, () => Array.from({ length: e[0].length }, () => [-1, -1] as Pair));
        q.push(t);
        trace[t[0]][t[1]] = [-2, -2];
        e[s[0]][s[1]] = 0;
        e[t[0]][t[1]] = 0;

        while (q.length > 0) {
            const u: Pair = q.shift()!;
            if (u[0] === s[0] && u[1] === s[1]) break;
            for (let i = 0; i < 4; ++i) {
                let nx = u[0] + dx[i];
                let ny = u[1] + dy[i];
                while (nx >= 0 && nx < e.length && ny >= 0 && ny < e[0].length && e[nx][ny] === 0) {
                    if (trace[nx][ny][0] === -1) {
                        trace[nx][ny] = u;
                        q.push([nx, ny]);
                    }
                    nx += dx[i];
                    ny += dy[i];
                }
            }
        }

        // Trace back
        const res: Pair[] = [];
        if (trace[s[0]][s[1]][0] !== -1) {
            while (s[0] !== -2) {
                res.push([s[0] - 1, s[1] - 1]);
                const [nx, ny] = trace[s[0]][s[1]];
                s[0] = nx;
                s[1] = ny;
            }
        }
        return res;
    }

    canConnect(pointStart: Vec2, pointEnd: Vec2): Vec2[] | null {
        let path = this.findPath(pointStart.x, pointStart.y, pointEnd.x, pointEnd.y);
        if (path.length >= 2 && path.length <= 4) {
            return path.map((p) => { return v2(p[0], p[1]) });
        }
        else return null;
    }

    private updateMoveToCenterHorizontally(row: number): number[][] {
        let values = this._matrix[row];
        let center = values.length / 2;
        for (let i = center - 1; i >= 0; i--) {
            if (this._mapFrame.get(this._matrix[row][i]) === -1) {
                // update all positions less than center go to nearest i position
                for (let j = i; j > 0; j--) {
                    let t = values[j];
                    values[j] = values[j - 1];
                    values[j - 1] = t
                }
            }
            let negI = values.length - (i + 1);
            if (this._mapFrame.get(this._matrix[row][negI]) === -1) {
                // update all positions greater than center go to nearest i position
                for (let j = negI; j < values.length - 1; j++) {
                    let t = values[j];
                    values[j] = values[j + 1];
                    values[j + 1] = t;
                }
            }
        }
        this._matrix[row] = values;
        return this._matrix;
    }
    private updateMoveToCenterVertically(col: number): number[][] {
        let matrix = this.transpose(this._matrix);
        let values = matrix[col];
        let center = values.length / 2;
        for (let i = center - 1; i >= 0; i--) {
            if (this._mapFrame.get(matrix[col][i]) === -1) {
                // update all positions less than center go to nearest i position
                for (let j = i; j > 0; j--) {
                    let t = values[j];
                    values[j] = values[j - 1];
                    values[j - 1] = t;
                }
            }
            let negI = values.length - (i + 1);
            if (this._mapFrame.get(matrix[col][negI]) === -1) {
                // update all positions greater than center go to nearest i + 1 position
                for (let j = negI; j < values.length - 1; j++) {
                    let t = values[j];
                    values[j] = values[j + 1];
                    values[j + 1] = t;
                }
            }
        }
        matrix[col] = values;
        this._matrix = this.transpose(matrix);
        return this._matrix;
    }

    private transpose(matrix: number[][]): number[][] {
        const rows = matrix.length;
        const cols = matrix[0].length;
        const transposed: number[][] = [];
    
        for (let col = 0; col < cols; col++) {
            transposed[col] = [];
            for (let row = 0; row < rows; row++) {
                transposed[col][row] = matrix[row][col];
            }
        }
    
        return transposed;
    }
}

export type Pair = [number, number];





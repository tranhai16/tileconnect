import { sys } from "cc";
import { AudioSource, AudioClip, resources, Node } from "cc";

export default class AudioManager {


    private static _instance: AudioManager;
    public static get instance(): AudioManager {
        if (!AudioManager._instance) {
            AudioManager._instance = new AudioManager();
        }
        return AudioManager._instance;
    }

    private _root: Node = null;
    private _audioSources: Map<string, AudioSource>;
    private _muteData: Map<string, boolean>;
    private _muteAll: boolean = true;
    public get muteAll(): boolean {
        return this._muteAll;
    }
    public set muteAll(value: boolean) {
        this._muteAll = value;
        if (this._muteAll) {
            this.stopAll();
        }
        else {
            this.resumeAll();
        }
    }
    private constructor() {
        this._audioSources = new Map<string, AudioSource>();
        this._muteData = new Map<string, boolean>();
    }

    init(root: Node): void {
        this._root = root;
    }

    addAudio(key: string, audioClip: string | AudioClip, loop: boolean = false, playOnWake: boolean = false): void {
        if (!this._root) {
            console.error("Root node not found. Please set the root node by init method before adding audio clips.");
            return;
        }
        if (this._audioSources.has(key)) {
            console.warn(`Audio clip with key ${key} already exists.`);
            return;
        }
        if (audioClip instanceof AudioClip) {
            this._addAudioSourceNode(key, audioClip, loop, playOnWake);
        }
        else {
            resources.load(audioClip, AudioClip, (err, ac) => {
                if (err) {
                    console.error(`Failed to load audio clip ${audioClip} from ${audioClip}.`, err);
                    return;
                }
                this._addAudioSourceNode(key, ac, loop, playOnWake);
            });
        }

    }
    private _addAudioSourceNode(key: string, audioClip: AudioClip, loop: boolean = false, playOnWake: boolean = false) {
        let storageSoundData = sys.localStorage.getItem(key);
        console.log("init audio data", key, ":", storageSoundData)
        if (!storageSoundData) {
            this._muteData.set(key, true);
            sys.localStorage.setItem(key, "on");
        }
        else {
            this._muteData.set(key, storageSoundData === "on" ? true : false);
        }
        let node = new Node(audioClip.name);
        node.addComponent(AudioSource);
        node.getComponent(AudioSource).clip = audioClip;
        node.getComponent(AudioSource).playOnAwake = this._muteData.get(key) && playOnWake;
        node.getComponent(AudioSource).loop = loop;
        node.getComponent(AudioSource).stop();
        this._root.addChild(node);
        this._audioSources.set(key, node.getComponent(AudioSource));
        console.log(`Added audio clip ${audioClip.name} to the audio manager.`);
    }

    play(key: string, loop: boolean = false) {
        if (this._muteAll) return;
        if (this._audioSources.has(key) && this._muteData.get(key) === true) {
            this._audioSources.get(key).loop = loop;
            this._audioSources.get(key).play();
        } else {
            if (!this._audioSources.has(key)) {
                console.warn(`Audio clip with key ${key} not found`);
            }
        }
    }

    isMute(key: string): boolean {
        if (this._audioSources.has(key)) {
            return this._muteData.get(key);
        } else {
            console.warn(`Audio clip with key ${key} not found.`);
            return true;
        }
    }
    mute(key: string, value: boolean) {
        if (this._audioSources.has(key)) {
            this._muteData.set(key, value);
            if(!value) this._audioSources.get(key).stop();
            sys.localStorage.setItem(key, value ? 'on' : 'off');
            value && this._audioSources.get(key).loop && this._audioSources.get(key).play()
        } else {
            console.warn(`Audio clip with key ${key} not found.`);
        }
    }

    stop(key: string) {
        if (this._audioSources.has(key)) {
            this._audioSources.get(key).stop();
        } else {
            console.warn(`Audio clip with key ${key} not found.`);
        }
    }

    resumeAll() {
        this._audioSources.forEach((audioSource, key) => {
            if (audioSource.loop && this._muteData.get(key)) {
                audioSource.play();
            }
        });
    }

    stopAll() {
        this._audioSources.forEach((audioSource, key) => {
            this.stop(key);
        });
    }
}
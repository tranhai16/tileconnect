import { sys } from "cc";
import { screen } from "cc";
import { UITransform } from "cc";
import { Size } from "cc";
import { Node } from "cc";
import { GameUtils, ShowEffect } from "./GameUtils";

export default class ViewUtils {

    private static _gameRoot: Node;
    public static get gameRoot(): Node {
        return ViewUtils._gameRoot;
    }
    public static set gameRoot(value: Node) {
        ViewUtils._gameRoot = value;
    }

    private static _loadingView: Node;
    static scaleNodeSizeToScreenSize(node: Node) {
        screen.windowSize.width
        let size = new Size(screen.windowSize.width, screen.windowSize.height);
        node.getComponent(UITransform)?.setContentSize(size);
    }

    static showLoading() {
        GameUtils.loadPrefabToNode('prefabs/loading', this._gameRoot, ShowEffect.OPACITY).then((node: Node) => {
            ViewUtils._loadingView = node;
        }); 
    }

    static hideLoading() {
        if (ViewUtils._loadingView) {
            ViewUtils._loadingView.destroy();
            ViewUtils._loadingView = null;
        }
    }

}
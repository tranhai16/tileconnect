export interface Level {
    level: number;
    rows: number;
    cols: number;
    array_tiles: number[];
    max_time: number;
    go_center_horizontal: boolean;
    go_center_vertical: boolean;
}

export function generateLevels(totalLevels: number): Level[] {
    const levels: Level[] = [];
    const goCenterPatterns = [
        { horizontal: false, vertical: false },
        { horizontal: false, vertical: false },
        { horizontal: true, vertical: false },
        { horizontal: false, vertical: true },
        { horizontal: true, vertical: true },
    ];

    let level = 1;
    for (let i = 1; i <= totalLevels; i++) {
        const area = getIncreasingArea(i, totalLevels);
        const { rows, cols } = getRowsAndCols(area);
        const arrayTiles = generateArrayTiles(rows, cols);
        const maxTime = calculateMaxTime(rows, cols);
        const patternIndex = (level - 1) % goCenterPatterns.length;
        const { horizontal, vertical } = goCenterPatterns[patternIndex];

        levels.push({
            level,
            rows,
            cols,
            array_tiles: arrayTiles,
            max_time: maxTime,
            go_center_horizontal: horizontal,
            go_center_vertical: vertical,
        });
        level++;
    }

    return levels;
}

function getRandomEvenNumber(min: number, max: number): number {
    const range = Math.floor((max - min) / 2) + 1;
    return min + 2 * Math.floor(Math.random() * range);
}

function getIncreasingArea(level: number, totalLevels: number): number {
    const minArea = 128; // 6*6
    const maxArea = 128; // 16*8
    return minArea + Math.floor((maxArea - minArea) * (level - 1) / (totalLevels - 1));
}

function getRowsAndCols(area: number): { rows: number, cols: number } {
    const possibleRows = [16];
    const possibleCols = [8];
    let rows = possibleRows[0];
    let cols = area / rows;

    for (let r of possibleRows) {
        for (let c of possibleCols) {
            if (r * c === area) {
                rows = r;
                cols = c;
                break;
            }
        }
    }

    return { rows, cols: Math.floor(cols) };
}

function generateArrayTiles(rows: number, cols: number): number[] {
    const totalTiles = rows * cols;
    let remainingTiles = totalTiles;
    const arrayTiles: number[] = [];

    while (remainingTiles > 0) {
        let tile = getRandomEvenNumber(2, Math.min(remainingTiles, 4));
        if (remainingTiles - tile < 0) {
            tile = remainingTiles;
        }
        arrayTiles.push(tile);
        remainingTiles -= tile;
    }

    return arrayTiles;
}

function calculateMaxTime(rows: number, cols: number): number {
    return 8 * 60 + Math.floor((rows * cols) / 2);
}


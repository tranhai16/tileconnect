import { Vec2 } from "cc";

export default class Line {
    private _start: Vec2;
    public get start(): Vec2 {
        return this._start;
    }
    public set start(value: Vec2) {
        this._start = value;
    }
    private _end: Vec2;
    public get end(): Vec2 {
        return this._end;
    }
    public set end(value: Vec2) {
        this._end = value;
    }
    constructor(start: Vec2 = new Vec2(), end: Vec2 = new Vec2()) {
        this._start = start;
        this._end = end;
    }
}
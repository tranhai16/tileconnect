import { director } from "cc";
import { SceneAsset } from "cc";
import { Scene } from "cc";
import { SpriteFrame, resources, loader, JsonAsset, Prefab, instantiate, tween, Node, UITransform, v3, UIOpacity } from "cc";

export enum ShowEffect {
    OPACITY,
    FROM_BOTTOM,
    FROM_TOP,
}

export class GameUtils {
    static getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static getRandomFloat(min: number, max: number): number {
        return Math.random() * (max - min) + min;
    }

    static shuffleArray(array: any[]): void {
        for (let i = array.length - 1; i >= 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    static formatNumberWithCommas(num: number): string {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    static formatTime(time: number): string {
        const minutes = Math.floor(time / 60);
        const seconds = Math.floor(time % 60);
        return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    }

    static formatDistance(distance: number): string {
        const meters = Math.floor(distance);
        const centimeters = Math.floor((distance - meters) * 100);
        return `${meters}m ${centimeters}cm`;
    }

    static loadAnimalSprites(): Promise<SpriteFrame[]> {
        return new Promise<SpriteFrame[]>((resolve, reject) => {
            console.log("Loading resources...");
            resources.loadDir('textures/foods/', SpriteFrame, (err, data) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    console.log("Resource load failed");
                    return;
                }
                resolve(data);
                console.log("Resources loaded.");
            })
        });
    }
    static loadLevelConfig(): Promise<any> {

        return new Promise((resolve, reject) => {
            console.log("Loading level config...");
            loader.loadRes('data/LevelConfig', JsonAsset, (err, data) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load level config: ${err}`);
                    return;
                }
                resolve(data.json);
                console.log("Level config loaded.");
            })
        })
    }

    static loadGameConfig(callback: (data: { name: string, scene: string }[]) => void) {
        this.loadJson('data/GameConfig').then((data) => {
            let results: { name: string, scene: string}[] = [];
            data.forEach((sceneData: { name: string, scene: string }) => {
                results.push(sceneData);
            });
            callback(results);
        })
    }

    static loadJson(path: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            resources.load(path, JsonAsset, (err, jsonAsset) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load json: ${path}, ${err}`);
                    return;
                }
                resolve(jsonAsset.json);
                console.log(`Json loaded:`, jsonAsset.json);
            })
        })
    }

    static loadPrefab(path: string): Promise<Prefab> {
        return new Promise<Prefab>((resolve, reject) => {
            console.log(`Loading prefab: ${path}`);
            resources.load(path, Prefab, (err, prefab) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load prefab: ${path}, ${err}`);
                    return;
                }
                resolve(prefab);
                console.log(`Prefab loaded: ${path}`);
            })
        })
    }
    static loadPrefabToNode(path: string, parent: Node, showEffect: ShowEffect = ShowEffect.OPACITY): Promise<Node> {
        return new Promise<Node>((resolve, reject) => {
            console.log(`Loading prefab: ${path}`);
            resources.load(path, Prefab, (err, prefab) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load prefab: ${path}, ${err}`);
                    return;
                }
                const node = instantiate(prefab);
                parent.addChild(node);
                switch (showEffect) {

                    case ShowEffect.FROM_TOP:
                        node.setPosition(0, parent.getComponent(UITransform).height);
                        tween(node).to(0.2, { position: v3(0, 0, 0) }, { easing: "quadInOut" }).call(() => {
                            resolve(node);
                        }).start();
                        break;
                    case ShowEffect.FROM_BOTTOM:
                        node.setPosition(0, -parent.getComponent(UITransform).height);
                        tween(node).to(0.2, { position: v3(0, 0, 0) }, { easing: "quadInOut" }).call(() => {
                            resolve(node);
                        }).start();
                        break;
                    case ShowEffect.OPACITY:
                    default:
                        let opacity = parent.getComponent(UIOpacity);
                        if (!opacity) {
                            opacity = node.addComponent(UIOpacity);
                        }
                        opacity.opacity = 0;
                        tween(opacity).to(0.2, { opacity: 255 }, { easing: "fade" }).call(() => {
                            resolve(node);
                        }).start();
                        break;

                }
                console.log(`Prefab loaded: ${path}`);
            })
        })
    }
}



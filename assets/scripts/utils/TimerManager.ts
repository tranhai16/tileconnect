import TimerProgress from '../components/TimerProgress';
export default class TimerManager {
    private static _instance: TimerManager;
    private _maxTime: number;
    private _counter: number;
    private _running: boolean = false;
    private _listener: TimerProgressListener;
    public set listener(value: TimerProgressListener) {
        this._listener = value;
    }
    public static get instance(): TimerManager {
        if (!TimerManager._instance) {
            TimerManager._instance = new TimerManager();
        }
        return TimerManager._instance;
    }

    private constructor() {
    }

    setup(maxTime: number, listener?: TimerProgressListener) {
        this._counter = 0;
        this._maxTime = maxTime;
        this._listener = listener;
    }

    start() {
        this._running = true;
        this._counter = 0;
        this._listener && this._listener.onStart && this._listener.onStart();
    }

    pause() {
        this._running = false;
        this._listener && this._listener.onPause && this._listener.onPause();
    }
    resume() {
        this._running = true;
        this._listener && this._listener.onResume && this._listener.onResume();
    }
    stop() {
        this._running = false;
        this._counter = this._maxTime;
        this._listener && this._listener.onEnd && this._listener.onEnd();
    }

    update(time: number) {
        if (this._running) {
            this._counter += time;
            this._listener && this._listener.onProgress && this._listener.onProgress(this._counter / this._maxTime, this._counter, this._maxTime);
            if (this._counter >= this._maxTime) {
                this.stop();
            }
        }
    }

    get progress(): number {
        return this._counter / this._maxTime;
    }
}

export type TimerProgressListener = {
    onStart?: () => void,
    onProgress?: (progress: number, count: number, total: number) => void,
    onEnd?:()=>void,
    onPause?:()=>void,
    onResume?:()=>void,
}
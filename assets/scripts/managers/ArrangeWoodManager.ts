import { Vec3 } from "cc";
import Wood from "../mvc/models/Wood";
import { Node } from "cc";
import { instantiate } from "cc";
import { Tile3DComponent } from "../views/Tile3DComponent";
import { GameUtils } from "../utils/GameUtils";
import { v3 } from "cc";

const config: { depths: number, rows: number, columns: number } = {
    depths: 6,
    rows: 6,
    columns: 6
}
export default class ArrangeWoodManager {
    matrix: number[][][];
    private _genId: number[] = [];
    private readonly _mapNode: Map<Vec3, number> = new Map<Vec3, number>();
    generateId(): number {
        if (this._genId.length === 0) {
            this._genId.push(1);
            return 1;
        }
        let id = this._genId[this._genId.length - 1] + 1;
        this._genId.push(id);
        return id;
    }

    private static _instance: ArrangeWoodManager;
    public static get instance(): ArrangeWoodManager {
        if (!ArrangeWoodManager._instance) {
            ArrangeWoodManager._instance = new ArrangeWoodManager();
        }
        return ArrangeWoodManager._instance;
    }

    private _currentTile: Node;
    public get currentTile(): Node {
        return this._currentTile;
    }
    public set currentTile(value: Node) {
        this._currentTile = value;
        if (!value) return;
        this._previewTile.getComponent(Tile3DComponent).init(value.getComponent(Tile3DComponent).wood);
    }

    private _previewTile: Node;
    public get previewTile(): Node {
        return this._previewTile;
    }

    deltaTime: number = 0;
    readonly viewPositionAddition: Vec3 = new Vec3(
        - config.rows / 2 + 0.5,
        0.5,
        - config.depths / 2 + 0.5
    );

    speed: Vec3 = new Vec3();

    private constructor() {
    }

    private arrayKeys: Array<Vec3>;

    init() {
        this._genId = [];
        this._mapNode.clear();
        this.matrix = Array.from({ length: config.depths }, () =>
            Array.from({ length: config.rows }, () =>
                Array.from({ length: config.columns }, () => 0)
            )
        );

        for (let z = 0; z < config.depths; z++) {
            for (let x = 0; x < config.rows; x++) {
                for (let y = 0; y < config.columns; y++) {
                    this._mapNode.set(new Vec3(x, y, z), this.matrix[z][x][y]);
                }
            }
        }

        this.arrayKeys = Array.from(this._mapNode.keys());


    }

    async setupPreview(): Promise<Node> {
        let previewPrefab = await GameUtils.loadPrefab('prefabs/preview_tiles');
        const previewTile = instantiate(previewPrefab);
        previewTile.active = false;
        this._previewTile = previewTile;
        return previewTile;
    }

    async drawPreview(): Promise<Node[]> {
        let woodPrefab = await GameUtils.loadPrefab('prefabs/preview_box');
        let tiles: Node[] = [];
        this._mapNode.forEach((value, key) => {
            let tile = instantiate(woodPrefab);
            tile.setPosition(key.clone().add(this.viewPositionAddition));
            tiles.push(tile);
        })
        return tiles;
    }


    add(wood: Wood, origin: Vec3) {
        let positions = wood.allPosition();
        let realInManagerPositions: Vec3[] = [];
        positions.forEach(position => {
            let pos = position.subtract3f(1, 1, 1);
            realInManagerPositions.push(pos.add(origin));
        });
        for (let index = 0; index < realInManagerPositions.length; index++) {
            let position = realInManagerPositions[index];
            if (this.matrix[position.z][position.x][position.y] !== 0) {
                console.log("has tile!");
                return;
            }
        }
        realInManagerPositions.forEach(position => {
            let woodPos = position.clone().subtract(origin).add3f(1, 1, 1);
            this.set(position, wood.get(woodPos.x, woodPos.y, woodPos.z));
        });
    }

    set(position: Vec3, value: number) {
        this.matrix[position.z][position.x][position.y] = value;
        this._mapNode.set(position, value);
    }

    canAdd(wood: Wood, origin: Vec3): boolean {
        let positions = wood.allPosition();
        console.clear();
        console.log("origin:", origin);
        for (let i = 0; i < positions.length; i++) {
            let position = positions[i];
            console.log("position:", position);
            let realPosition = position.clone().subtract3f(1, 1, 1).add(origin);
            console.log("realPosition", i, " ", realPosition.clone());
            if (realPosition.x < 0
                || realPosition.x >= config.rows
                || realPosition.y < 0
                || realPosition.y >= config.columns
                || realPosition.z < 0
                || realPosition.z >= config.depths) {
                console.log("out of range!");
                return false;
            }
            if (this.matrix[realPosition.z][realPosition.x][realPosition.y] !== 0) {
                console.log("has tile!");
                return false;
            }
        }
        return true;
    }

    async spawn(parent: Node): Promise<Node> {
        let prefab = await GameUtils.loadPrefab('prefabs/tile3');
        const tile = instantiate(prefab);
        tile.parent = parent;
        tile.getComponent(Tile3DComponent).init(new Wood());
        // this.currentTile = tile;
        return tile;
    }


    updatePreview() {
        this._previewTile.active = false;
        // let arrKeys = Array.from(this._mapNode.keys());
        let currentTilePosition = this._currentTile.getPosition();
        let arrayDistances: { key: Vec3, distance: number }[] = [];
        this.arrayKeys.forEach(key => {
            let distance = Vec3.distance(currentTilePosition, key.clone().add(this.viewPositionAddition));
            arrayDistances.push({ key, distance });
        })
        arrayDistances.sort((a, b) => a.distance - b.distance);
        let nearest = arrayDistances[0];
        if (!nearest) return;
        let currentWood = this._currentTile.getComponent(Tile3DComponent).wood;
        if (this.canAdd(currentWood, nearest.key)) {
            this._previewTile.getComponent(Tile3DComponent).init(currentWood);
            this._previewTile.setPosition(nearest.key.clone().add(this.viewPositionAddition));
            this._previewTile.active = true;
        }
    }

    moveUp(value: number = 10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(0, this.deltaTime * value, 0)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
    moveDown(value: number = -10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(0, this.deltaTime * value, 0)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
    moveLeft(value: number = -10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(this.deltaTime * value, 0, 0)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
    moveRight(value: number = 10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(this.deltaTime * value, 0, 0)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
    moveFront(value: number = 10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(0, 0, this.deltaTime * value)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
    moveBack(value: number = -10) {
        let currentTilePosition = this._currentTile.getPosition();
        this._currentTile.setPosition(currentTilePosition.add(v3(0, 0, this.deltaTime * value)));
        this.updatePreview();
        // console.clear();
        // this._currentTile.getComponent(Tile3DComponent).wood.allPosition().forEach((position) => {
        //     console.log(position.clone().subtract3f(1, 1, 1));
        // })
    }
}
import { sys } from "cc";
import { InterstitialAdClient } from "../../../extensions/Google AdMob/assets/ads/client/InterstitialAdClient";
import { RewardedInterstitialAdClient } from "../../../extensions/Google AdMob/assets/ads/client/RewardedInterstitialAdClient";
import ViewUtils from "../utils/ViewUtils";
import AdConst from "../const/AdConst";

export default class AdmobManager {
    private static _instance: AdmobManager;
    public static get instance(): AdmobManager {
        if (!AdmobManager._instance) {
            AdmobManager._instance = new AdmobManager();
        }
        return AdmobManager._instance;
    }

    private _interstitialAds: InterstitialAdClient = null;
    private _rewardedInterstitialAds: RewardedInterstitialAdClient = null;

    private constructor() {

    }

    showInterAds(type: string, callback : () => void) : void {
        if (this._interstitialAds) return;
        this._interstitialAds = new InterstitialAdClient();
        const self = this;
        this._interstitialAds.load(type, {
            onAdLoaded() {
                self._interstitialAds.show(() => {
                    callback();
                })
            },
            onAdFailedToLoad(loadAdError) {
                console.log('Load interstitial ads failed', loadAdError);
                self._interstitialAds = null;
            },
            onAdShowedFullScreenContent() {
            },
            onAdDismissedFullScreenContent() {
                self._interstitialAds = null;
            },
            onAdFailedToShowFullScreenContent(adError) {
                self._interstitialAds = null;
            },
            onPaidEvent(paidNTF) {
                console.log('Paid event paid', paidNTF);
            },
        })
    }
    showRewardedAds(type: string, callback : (data: {type: string, amount: number}) => void) : void {
        // @ts-ignore
        if (AdConst.TEST) {
            console.log("this is debug")
            callback({type: type, amount:1});
            return;
        }
        if (this._rewardedInterstitialAds) return;
        this._rewardedInterstitialAds = new RewardedInterstitialAdClient();
        const self = this;
        ViewUtils.showLoading();
        this._rewardedInterstitialAds.load(type, {
            onAdLoaded(unitId) {
                console.log("rewarded ads loaded", unitId);
                ViewUtils.hideLoading();
                self._rewardedInterstitialAds.show();
            },
            onEarn(rewardType, amount) {
                callback({
                    type: rewardType,
                    amount: amount
                });
                self._rewardedInterstitialAds = null;
            },
            onAdFailedToLoad(loadAdError) {
                self._rewardedInterstitialAds = null;
                console.log(loadAdError);
                callback(null);
            },
            onAdDismissedFullScreenContent() {
                self._rewardedInterstitialAds = null;
            },
            onAdFailedToShowFullScreenContent(adError) {
                self._rewardedInterstitialAds = null;
                console.log(adError);
                callback(null);
            },
        })
    }
}
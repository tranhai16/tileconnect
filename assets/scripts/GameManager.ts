import { Prefab, SpriteFrame, EventTarget, Vec2, Node, v2, UITransform } from "cc";
import Algorithm from "./utils/Algorithm";
import LevelConfig from "./utils/LevelConfig";
import { Size } from "cc";
import { sys } from "cc";
import TileController from "./mvc/controllers/TileController";

const eventTarget: EventTarget = new EventTarget();

export default class GameManager {

    private static _instance: GameManager;
    private tilePrefab: Prefab;
    private readonly _userData: {
        current_level: number,
        current_score: number,
        best_score: number,
        current_hint_number: number
    } = {
            current_level: 1,
            current_score: 0,
            best_score: 0,
            current_hint_number: 0
        };
    public get userData(): {
        current_level: number,
        current_score: number,
        best_score: number,
        current_hint_number: number
    } {
        return this._userData;
    }
    private readonly tileSize: Size = new Size();
    public get eventTarget(): EventTarget {
        return eventTarget;
    }
    public static get instance(): GameManager {
        if (!this._instance) this._instance = new GameManager();
        return GameManager._instance;
    }

    private _drawNode: Node;
    private _tileConnectIdx: number[] = [];
    spriteFrameData: SpriteFrame[] = [];
    levelConfigs: LevelConfig[] = [];
    currentLevelData: LevelConfig;
    private _tileController: TileController;
    private constructor() {
        let user_data = sys.localStorage.getItem("user-data");

        if (!user_data) {
            sys.localStorage.setItem("user-data", JSON.stringify(this.userData));
        }
        else {
            let data = JSON.parse(user_data);
            this._userData.current_level = data.current_level ? data.current_level : 1;
            this._userData.current_score = data.current_score ? data.current_score : 0;
            this._userData.best_score = data.best_score ? data.best_score : 0;
            this._userData.current_hint_number = data.current_hint_number ? data.current_hint_number : 0;
        }
    }

    initPrefab(prefab: Prefab, drawNode: Node) {
        this.tilePrefab = prefab;
        this._drawNode = drawNode;
        let drawSize = drawNode.getComponent(UITransform).contentSize;
        this.tileSize.set(drawSize.width / 16, drawSize.height / 8);
        console.log("--------------------------------");
        console.log("Animal prefab inited");
        console.log("--------------------------------");
    }
    showHint(callback: (rewarded: boolean) => void) {
        this._tileController.showHint(this._userData.current_hint_number, (rw) => {
            if (rw) {
                this.userData.current_hint_number++;
                
            }
            else {
                if (this.userData.current_hint_number > 0) {
                    this.userData.current_hint_number--;
                }
            }
            sys.localStorage.setItem("user-data", JSON.stringify(this._userData));
                eventTarget.emit("hint-change");
            callback(rw);
        });
    }
    timeEnded() {
        if (!Algorithm.instance.isEmpty) {
            console.log('Game over!');
            eventTarget.emit('game-over');
            return;
        }
        console.log('Congratulations! You won the game!');
        this._userData.current_level++;
        sys.localStorage.setItem('user-data', JSON.stringify(this._userData));
        eventTarget.emit('new-game');
    }

    public async initGame(): Promise<void> {
        this._drawNode.removeAllChildren();
        if (!this.tilePrefab) return;
        this.currentLevelData = this.levelConfigs.find(data => data.level === this._userData.current_level);
        let rows = this.currentLevelData.rows;
        let cols = this.currentLevelData.cols;
        Algorithm.instance.generate(rows, cols, this.currentLevelData.array_tiles.length, this.currentLevelData.array_tiles);
        Algorithm.instance.setGoCenter(this.currentLevelData.go_center_horizontal, this.currentLevelData.go_center_vertical);
        this._tileController = new TileController(Algorithm.instance, this.tilePrefab, this.tileSize);
        this._tileController.views.forEach((view) => {
            let frame = this.spriteFrameData.find((sp) => sp.name === view.frameData.toString());
            if (frame) {
                view.setSpriteFrame(frame);
            }
            this._drawNode.addChild(view.node);
        });
        GameManager.instance.eventTarget.emit("update-score");
    }

    updateUI() {
        if (Algorithm.instance.isEmpty) {
            this.timeEnded();
        }
        
    }
    public startGame(): void {
        // Implement your game start logic here
    }

}



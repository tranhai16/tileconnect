import { _decorator, Component, Node } from 'cc';
import GameManager from '../GameManager';
import { Label } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('HintView')
export class HintView extends Component {

    @property(Node)
    adsLabelNode : Node;
    @property(Node)
    hintNumberNode : Node;
    start() {
        this.onHintNumberChanged();
    }
    protected onEnable(): void {
        GameManager.instance.eventTarget.on('hint-change', this.onHintNumberChanged, this);
    }
    protected onDisable(): void {
        GameManager.instance.eventTarget.off('hint-change', this.onHintNumberChanged, this);
    }

    onHintNumberChanged() {
        this.hintNumberNode.getComponent(Label).string = GameManager.instance.userData.current_hint_number.toString();
        this.adsLabelNode.active = GameManager.instance.userData.current_hint_number === 0;

    }

    update(deltaTime: number) {
        
    }
}



// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import { _decorator, Component, Graphics, Vec2, Node } from "cc";
import TimerProgress from "../components/TimerProgress";
import GameManager from "../GameManager";
import AudioManager from "../utils/AudioManager";
import { AuidoKey } from "../utils/Const";
import { GameUtils, ShowEffect } from "../utils/GameUtils";
import { Label } from "cc";
import TimerManager from "../utils/TimerManager";

const {ccclass, property} = _decorator;

@ccclass
export default class MainView extends Component {
    
    @property(Graphics)
    lineDraw: Graphics;
    @property(Node)
    readyNode: Node;
    @property(Node)
    startNode: Node;
    @property(Node)
    gameOver: Node;
    @property(Label)
    currentLevel: Label;
    @property(Label)
    currentScore: Label;
    @property(Node)
    buttons: Node;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        TimerManager.instance.listener = {
            onStart() {
                
            },
            onEnd() {
                GameManager.instance.timeEnded();
            },
        }
    }
    showBtnStart() {
        this.startNode.active = true;
    }
    protected onEnable(): void {
        GameManager.instance.eventTarget.on('draw-line', this.drawLine, this);
        GameManager.instance.eventTarget.on('clear-line', this.clearLine, this);
        GameManager.instance.eventTarget.on('play-exellent', this.playExellent, this);
        GameManager.instance.eventTarget.on('play-wrong', this.playWrong, this);
        GameManager.instance.eventTarget.on('game-over', this.onGameOver, this);
        GameManager.instance.eventTarget.on('update-score', this.updateScore, this);
        
    }
    
    start () {
    }

    updateScore() {
        this.currentScore.string = `Score: ${GameManager.instance.userData.current_score}`;
    }

    playGame() {
        this.readyNode.active = this.gameOver.active = this.startNode.active = false;
        this.buttons.active = !this.readyNode.active;
        TimerManager.instance.start();
        
    }

    playAgain() {
        this.gameOver.active = false;
        this.reloadUI();
    }
    reloadUI() {
        GameManager.instance.initGame();
        this.readyNode.active = true;
        this.buttons.active = !this.readyNode.active;
        let progressNode = this.node.getChildByName("progress");
        progressNode.removeAllChildren();
        GameUtils.loadPrefabToNode('prefabs/time_progress', progressNode, ShowEffect.FROM_TOP).then((node: Node) => {
            TimerManager.instance.setup(GameManager.instance.currentLevelData.max_time);
            this.currentLevel!.string = 'Level: ' + GameManager.instance.userData.current_level;
            this.showBtnStart();
        })
    }
    onGameOver() {
        this.gameOver.active = true;
    }
    // update (dt) {}

    playExellent () {
        // this.exellentSource.play();
        AudioManager.instance.play(AuidoKey.EXELLENT);

    }
    playWrong () {
        // this.wrongSource.play();
        AudioManager.instance.play(AuidoKey.WRONG);
    }


    drawLine(points: Vec2[]) {
        this.lineDraw.clear();
        this.lineDraw.lineWidth = 5;
        // this.lineDraw.strokeColor.fromHEX('#ff0000');
        this.lineDraw.moveTo(points[0].x, points[0].y);
        for (let i = 1; i < points.length; i++) {
            this.lineDraw.lineTo(points[i].x, points[i].y);
        }
        this.lineDraw.stroke();
    }

    clearLine() {
        this.lineDraw.clear()
    }
    showHint() {
        TimerManager.instance.pause();
        GameManager.instance.showHint((rewarded => {
            TimerManager.instance.resume();
        }));
    }

    showSettings() {
        GameUtils.loadPrefabToNode('prefabs/setting_bg', this.node, ShowEffect.OPACITY);
    }

    protected update(dt: number): void {
        TimerManager.instance.update(dt);
    }
    protected onDisable(): void {
        GameManager.instance.eventTarget.off('draw-line', this.drawLine, this);
        GameManager.instance.eventTarget.off('play-exellent', this.playExellent, this);
        GameManager.instance.eventTarget.off('play-wrong', this.playWrong, this);
        GameManager.instance.eventTarget.off('game-over', this.onGameOver, this);
        GameManager.instance.eventTarget.off('update-score', this.updateScore, this);
    }

    exit() {

    }

    addTime() {
        
    }
}

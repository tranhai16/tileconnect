import { _decorator, Component, Node } from 'cc';
import Wood from '../mvc/models/Wood';
import { Prefab } from 'cc';
import { instantiate } from 'cc';
import { CCBoolean } from 'cc';
import { tween } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Tile3DComponent')
export class Tile3DComponent extends Component {
    rotateX(arg0: number) {
        this.wood.rotateX(arg0);
        this.mapChild.forEach((child, id) => {
            tween(child).to(0.5, { position: this.wood.position(id)!.subtract3f(1, 1, 1) }, { easing: 'backInOut' }).start();
        })
    }
    rotateY(arg0: number) {
        this.wood.rotateY(arg0);
        this.mapChild.forEach((child, id) => {
            tween(child).to(0.5, { position: this.wood.position(id)!.subtract3f(1, 1, 1) }, { easing: 'backInOut' }).start();
        })
    }
    rotateZ(arg0: number) {
        this.wood.rotateZ(arg0);
        this.mapChild.forEach((child, id) => {
            tween(child).to(0.5, { position: this.wood.position(id)!.subtract3f(1, 1, 1) }, { easing: 'backInOut' }).start();
        })
    }
    updateMesh() {
        this.node.removeAllChildren();
        if (!this.boxWoodPrefab) return;
        for (let z = 0; z < this.wood.matrix.length; z++) {
            for (let x = 0; x < this.wood.matrix[z].length; x++) {
                for (let y = 0; y < this.wood.matrix[z][x].length; y++) {
                    if (this.wood.matrix[z][x][y] !== 0) {
                        this.createBoxWood(x, y, z);
                    }
                }
            }
        }
    }

    @property(Prefab)
    boxWoodPrefab: Prefab;

    private _test: boolean = false;
    public get test(): boolean {
        return this._test;
    }
    @property({ type: CCBoolean })
    public set test(value: boolean) {
        this._test = value;
        this.init(new Wood());
    }

    private mapChild: Map<number, Node>;
    wood: Wood;

    init(wood: Wood) {
        this.wood = wood;
        this.mapChild = new Map<number, Node>();
        this.updateMesh();

    }
    createBoxWood(x: number, y: number, z: number) {
        const woodNode = instantiate(this.boxWoodPrefab);
        woodNode.parent = this.node;
        woodNode.setPosition(x - 1, y - 1, z - 1);
        this.mapChild.set(this.wood.get(x, y, z), woodNode);
    }
    start() {

    }

    update(deltaTime: number) {

    }
}



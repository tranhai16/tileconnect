import { _decorator, Component, Node } from 'cc';
import { SwitchComponent } from '../components/SwitchComponent';
import { sys } from 'cc';
import AudioManager from '../utils/AudioManager';
import { AuidoKey } from '../utils/Const';
import TimerManager from '../utils/TimerManager';
const { ccclass, property } = _decorator;

@ccclass('SettingPopup')
export class SettingPopup extends Component {

    @property(SwitchComponent)
    musicBtnSwitch: SwitchComponent;
    @property(SwitchComponent)
    soundBtnSwitch: SwitchComponent;

    protected onLoad(): void {
        let hasMusic = sys.localStorage.getItem(AuidoKey.MUSIC);
        let hasSound = sys.localStorage.getItem(AuidoKey.CLICK);

        this.musicBtnSwitch.isOn = hasMusic ? (hasMusic === 'on' ? true : false) : false;
        this.soundBtnSwitch.isOn = hasSound ? (hasSound === 'on' ? true : false) : false;

        this.musicBtnSwitch.onChangeListener = (value: boolean) => {
            AudioManager.instance.mute(AuidoKey.MUSIC, value);
        }
        this.soundBtnSwitch.onChangeListener = (value: boolean) => {
            AudioManager.instance.mute(AuidoKey.CLICK, value);
            AudioManager.instance.mute(AuidoKey.EXELLENT, value)
            AudioManager.instance.mute(AuidoKey.WRONG, value);
        }
    }
    protected onEnable(): void {
        TimerManager.instance.pause();
    }
    start() {

    }

    update(deltaTime: number) {

    }

    close() {
        TimerManager.instance.resume();
        this.node.removeFromParent();
    }
}



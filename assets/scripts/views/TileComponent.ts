import { Sprite, tween } from 'cc';
import { Tween, UIOpacity } from 'cc';
import { _decorator, Component, Node } from 'cc';
import GameManager from '../GameManager';
import AudioManager from '../utils/AudioManager';
import { AuidoKey } from '../utils/Const';
import TileView from '../mvc/views/TileView';
const { ccclass, property } = _decorator;

@ccclass('TileComponent')
export class TileComponent extends Component {

    
    private _tileView: TileView;
    public get tileView(): TileView {
        return this._tileView;
    }
    public set tileView(value: TileView) {
        this._tileView = value;
    }
    private showHintTween: Tween<UIOpacity>;

    @property(Node)
    hintNode: Node;
    @property(Sprite)
    bgSprite: Sprite;
    @property(Sprite)
    tileSprite: Sprite;

    private _onClickListener: () => void;
    public set onClickListener(value: () => void) {
        this._onClickListener = value;
    }

    start() {

    }

    update(deltaTime: number) {
        
    }

    onClick() {
        if (this._onClickListener) {
            this._onClickListener();
        }
        
    }

    gray() {
        this.bgSprite.grayscale = true;
    }

    normal() {
        this.bgSprite.grayscale = false;
    }

    showHint() {
        this.hintNode.active = true;
        let hintTween = tween(this.hintNode.getComponent(UIOpacity))
            .to(0.2, { opacity: 255 * 0.5 })
            .delay(0.1)
            .to(0.2, { opacity: 255 });
        this.showHintTween = tween(this.hintNode.getComponent(UIOpacity)).repeat(5 / 0.2, hintTween);
        this.showHintTween.call(() => {
            this.hideHint();
        }).start();
        
    }
    hideHint() {
        if (this.showHintTween) {
            this.showHintTween.stop();
            this.showHintTween.destroySelf();
            this.showHintTween = null;
        }
        this.hintNode!.active = false;
        this.hintNode!.getComponent(UIOpacity)!.opacity = 255;
    }
    remove() {
        this.hideHint();
        this.node.destroy();
    }
}



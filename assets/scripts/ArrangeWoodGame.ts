import { Camera } from 'cc';
import { _decorator, Component, Node } from 'cc';
import { Tile3DComponent } from './views/Tile3DComponent';
import { Prefab } from 'cc';
import { instantiate } from 'cc';
import { Quat } from 'cc';
import { Vec3 } from 'cc';
import { math } from 'cc';
import { tween } from 'cc';
import { TweenSystem } from 'cc';
import Wood from './mvc/models/Wood';
import ArrangeWoodManager from './managers/ArrangeWoodManager';
import { v3 } from 'cc';
import { EventTouch } from 'cc';
import { input } from 'cc';
import { Input } from 'cc';
import { geometry, PhysicsSystem } from 'cc';
import { Vec2 } from 'cc';
const { ccclass, property } = _decorator;
const bound: {
    x: { min: number, max: number },
    y: { min: number, max: number },
    z: { min: number, max: number },
} = {
    x: { min: -3, max: 3 },
    y: { min: 0, max: 9 },
    z: { min: -3, max: 3 },
}

const cameraView = ['z', 'x', 'y'];

@ccclass('ArrangeWoodGame')
export class ArrangeWoodGame extends Component {

    @property(Camera)
    camera: Camera;
    @property(Node)
    spawnNode: Node;
    @property(Prefab)
    tilePrefab: Prefab;
    @property(Node)
    root3dNode: Node;
    @property(Node)
    camPos1: Node;
    @property(Node)
    camPos2: Node;
    @property(Node)
    camPos3: Node;

    private _cameraFace: string = cameraView[0];
    private _beforeTouchPos: Vec2 = new Vec2();

    protected onLoad(): void {
        ArrangeWoodManager.instance.init();
        ArrangeWoodManager.instance.setupPreview().then((node) => {
            this.root3dNode.addChild(node);
        });
        // ArrangeWoodManager.instance.drawPreview().then((nodes) => {
        //     for (let node of nodes) {
        //         this.root3dNode.addChild(node);
        //     }
        // })
        this.updatePositionCamera(this.camPos1);
    }


    protected onEnable(): void {
        input.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
        input.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        input.on(Input.EventType.TOUCH_END, this.onTouchEnd, this);
    }
    protected onDisable(): void {
        input.off(Input.EventType.TOUCH_START, this.onTouchStart, this);
        input.off(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        input.off(Input.EventType.TOUCH_END, this.onTouchEnd, this);
    }
    onTouchStart(event: EventTouch) {
        if (ArrangeWoodManager.instance.currentTile) return;
        let ray = new geometry.Ray();
        this.camera.screenPointToRay(event.getLocationX(), event.getLocationY(), ray);
        // The following parameters are optional
        const mask = 0xffffffff;
        const maxDistance = 10000000;
        const queryTrigger = true;

        if (PhysicsSystem.instance.raycastClosest(ray, mask, maxDistance, queryTrigger)) {
            const raycastClosestResult = PhysicsSystem.instance.raycastClosestResult;
            const collider = raycastClosestResult.collider;
            if (collider) {
                // console.log(collider);
                ArrangeWoodManager.instance.currentTile = collider.node.parent.parent;
                this._beforeTouchPos.set(event.getUILocation());
            }
        }
    }
    onTouchMove(event: EventTouch) {
        if (!ArrangeWoodManager.instance.currentTile) return;
        let touchLocation = event.getUILocation();
        let change = touchLocation.clone().subtract(this._beforeTouchPos);
        // change.multiplyScalar(0.02);
        if (this._cameraFace === 'x' || this._cameraFace === 'z') {
            if (change.y < 0) {
                ArrangeWoodManager.instance.moveDown(change.y);
            }
            else {
                ArrangeWoodManager.instance.moveUp(change.y);
            }
            if (this._cameraFace === 'x') {
                if (change.x < 0) {
                    ArrangeWoodManager.instance.moveBack(change.x);
                }
                else {
                    ArrangeWoodManager.instance.moveFront(change.x);
                }
            }
            else {
                if (change.x < 0) {
                    ArrangeWoodManager.instance.moveLeft(change.x);
                }
                else {
                    ArrangeWoodManager.instance.moveRight(change.x);
                }
            }

        }
        else if (this._cameraFace === 'y') {
            if (change.x < 0) {
                ArrangeWoodManager.instance.moveFront(change.x);
            }
            else {
                ArrangeWoodManager.instance.moveBack(change.x);
            }
            if (change.y < 0) {
                ArrangeWoodManager.instance.moveLeft(change.y);
            }
            else {
                ArrangeWoodManager.instance.moveRight(change.y);
            }
        }
        this._beforeTouchPos.set(touchLocation);
    }
    onTouchEnd(event: EventTouch) {
        // ArrangeWoodManager.instance.currentTile = null;
    }

    spawn() {
        ArrangeWoodManager.instance.spawn(this.root3dNode).then((tile) => {
            tile.setPosition(this.spawnNode.getPosition());
        });
    }
    start() {

    }

    changeCameraView() {
        let index = cameraView.indexOf(this._cameraFace);
        index++;
        if (index >= cameraView.length) {
            index = 0;
        }
        this._cameraFace = cameraView[index];
        switch (this._cameraFace) {
            case 'z':
                this.updatePositionCamera(this.camPos1);
                break;
            case 'x':
                this.updatePositionCamera(this.camPos2);
                break;
            case 'y':
                this.updatePositionCamera(this.camPos3);
                break;
            default:
                break;
        }
    }

    switchCamera(ev, key: string) {
        switch (key) {
            case '1':
                this.updatePositionCamera(this.camPos1);
                this._cameraFace = 'z';
                break;
            case '2':
                this.updatePositionCamera(this.camPos2);
                this._cameraFace = 'x';
                break;
            case '3':
                this.updatePositionCamera(this.camPos3);
                this._cameraFace = 'y';
                break;
            default:
                this._cameraFace = '';
                break;
        }
    }

    updatePositionCamera(node: Node) {
        const self = this;
        let up = Vec3.UP;
        // let next_rotation = node.getRotation();
        if (node.getPosition().equals(this.camPos3.position)) {
            up = Vec3.UNIT_X;
        }
        // this.camera.node.setRotationFromEuler(v3(0, 0, 0));
        tween(this.camera.node).to(0.5, { position: node.getPosition()}, {
            easing: 'circOut', onUpdate(target, ratio) {
                self.camera.node.lookAt(v3(0, 0, 0), up);
            },
        }).call(() => {
            // if (node.equals(this.camPos3.position)) {
            //     this.camera.node.setRotationFromEuler(v3(-90, -90, 0));
            // }
        }).start();
    }

    update(deltaTime: number) {
        ArrangeWoodManager.instance.deltaTime = deltaTime;
    }

    onButtonMoveClick(ev, data) {
        if (!ArrangeWoodManager.instance.currentTile) return;
        let dir: string = data as string;
        switch (dir) {
            case 'left':
                ArrangeWoodManager.instance.moveLeft();
                break;
            case 'right':
                ArrangeWoodManager.instance.moveRight();
                break;
            case 'up':
                ArrangeWoodManager.instance.moveUp();
                break;
            case 'down':
                ArrangeWoodManager.instance.moveDown();
                break;
            case 'front':
                ArrangeWoodManager.instance.moveFront();
                break;
            case 'back':
                ArrangeWoodManager.instance.moveBack();
                break;
            default:
                break;
        }
        // this.updatePosition(tile);
    }
    onButtonRotateClick(ev, data: string) {
        let isReverse = data.includes('r');
        let angle = isReverse ? 90 : -90;
        const tile = ArrangeWoodManager.instance.currentTile;
        if (!tile) return;
        let tile3DComp = tile.getComponent(Tile3DComponent);
        if (data.includes('x')) {
            tile3DComp.rotateX(angle);
        }
        else if (data.includes('y')) {
            tile3DComp.rotateY(angle);
        }
        else if (data.includes('z')) {
            tile3DComp.rotateZ(angle);
        }
        else return;

    }
    // updatePosition(tile: Node) {
    //     let position = tile.getPosition();
    //     let halfSize = tile.getComponent(Tile3DComponent).meshCollider.worldBounds.halfExtents.clone();
    //     if (position.x + halfSize.x >= bound.x.max) position.x = bound.x.max - halfSize.x;
    //     if (position.x - halfSize.x <= bound.x.min) position.x = bound.x.min + halfSize.x;
    //     if (position.y + halfSize.y >= bound.y.max) position.y = bound.y.max - halfSize.y;
    //     if (position.y - halfSize.y <= bound.y.min) position.y = bound.y.min + halfSize.y;
    //     if (position.z - halfSize.z <= bound.z.min) position.z = bound.z.min + halfSize.z;
    //     if (position.z + halfSize.z >= bound.z.max) position.z = bound.z.max - halfSize.z;
    //     tile.setPosition(position);
    // }
}


